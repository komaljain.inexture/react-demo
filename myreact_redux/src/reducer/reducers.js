import {createStore} from "redux";

//reducer.js
const initialState={
  counter:0
}
export function reducer(state=initialState,action){
  switch(action.type){
    case 'INCREMENT':
    return{counter:state.counter + 1}
    case 'DECREMENT':
      return{counter:state.counter-1}
    default:
      return state;
  }
}

//store.js
export const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());