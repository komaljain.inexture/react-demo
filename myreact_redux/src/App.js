import React from 'react';
import {useSelector,useDispatch} from "react-redux";
import "./App.css";


// App.js
 function App(){
   const counter =useSelector(state=>state.counter);
   const dispatch = useDispatch();
   return(
     <div className ="App">
       <header className="App-header">
         <h1>Counter : <br /> {counter}</h1>
         <button className="button_INC" onClick={(()=>dispatch({type:"INCREMENT"}))}> INCREMENT</button>
         <button className="button_DEC" onClick={(()=>dispatch({type:"DECREMENT"}))}> DECREMENT</button>
       </header>
     </div>
   )
 }


 export default App;