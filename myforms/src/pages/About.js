import React from 'react';
import {useLocation,useHistory} from 'react-router';

function About(){
    const location = useLocation();
    const history = useHistory();
    console.log(location);

    function goBackHandle () {
        history.goBack();
    }
    return (
    <>
        <h1>About</h1>
        <h1>Location={location.pathname} </h1>
        <h1>From={location.state.from   } </h1>
        <button onClick ={goBackHandle}> Go Back </button>
    </>);
}
export default About;