import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from "react-router-dom";
import './index.css';
import Home from'./pages/Home';
import About from'./pages/About';
import User from './pages/User';
import Blog from './pages/Blog';



 function App() {
  

    return (
    
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              < NavLink to="/" exact activeStyle={ {color:'grey'}}>  Home</ NavLink>
            </li>
            <li>
              < NavLink to={
                {
                pathname : "/about",state : {from :"root"}}
              }  exact activeStyle={ {color:'grey'}}>About</ NavLink>
            </li>
            <li>
              < NavLink to="/user/Komal"   activeStyle={ {color:'grey'}}>User </ NavLink>
            </li>
            <li>
              < NavLink to="/blog/react"   activeStyle={ {color:'grey'}}>Blog </ NavLink>
            </li>
          </ul>
        </nav>


      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/about" component={About} />
        <Route exact path="/user/:name" component={User} />
        <Route exact path="/blog/:bname" component={Blog} />

      </Switch>
      </div>
    </Router>
    );
  }

  export default App;
