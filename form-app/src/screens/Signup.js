import  React from "react";
import { useForm } from "react-hook-form";
import {yupResolver} from '@hookform/resolvers/yup';
import {Link} from 'react-router-dom';
import {schema} from '../Validations/SignupValidation'
import { connect, useDispatch } from 'react-redux';
import { CallUserRegister } from "../reducer/actions";


    

const Signup = ()=> {
    const {register ,handleSubmit,formState:{errors},watch} = useForm({ resolver : yupResolver(schema),
      reValidateMode:"onChange",
      mode:"all"});
      const dispatch =useDispatch();   
    //Data storage to localstore
    const onSubmit = (data) =>  {
     
      dispatch(CallUserRegister(data))
    };
   

   
  console.log("all fields ========>", watch());
    
  return (
    
    <div className="outer">
    <form className="box" onSubmit={handleSubmit(onSubmit)} >
        <h3>Sign-Up</h3>
                <div className="form-group">
                    <label>Full name</label>
                    <input 
                    type="text" id="Fullname" className="form-control"  placeholder="Full name" name="name"
                    {...register("name")}
                     />
                     <p>{errors?.name?.message}</p>
                </div>
               

                <div className="form-group">
                    <label>Email</label>
                    <input  {...register("email")}
                     type="email" className="form-control" placeholder="Enter email" name="email"
                    
                    
                  />
                   <p>{errors?.email?.message}</p>
               
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input {...register("password")}
                    type="password" className="form-control" placeholder="Enter password" name ="password"
                   
                    
                   />
                    <p>{errors?.password?.message}</p>
                  

                </div>

                <button type="submit" className="btn btn-dark btn-lg btn-block"  >  Register</button>
             
                <p className="forgot-password text-right">
                    Already registered <Link to ="/sign-in">log in?</Link>
                </p>
    </form>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state,
  };
};

const mapDispatchToProps = {
  CallUserRegister,
};

export default connect(mapStateToProps, mapDispatchToProps)(Signup);