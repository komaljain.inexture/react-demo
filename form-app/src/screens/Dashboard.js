import React , { lazy, Suspense }  from 'react';
//import { useHistory} from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import { UserLogout } from "../reducer/actions";

const Newsapi = lazy(() => import('./Newsapi'))

// Get item from localstorage
const Dashboard =()=>{
    const dispatch =useDispatch();   
  //  const history =useHistory();
    let information=JSON.parse(localStorage.getItem("komal"));
    let user =information["name"];

    // Remove data after logout
function remove (){
    dispatch(UserLogout());
    
}
// Dashboard Nav
    return(
        <div>
       <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
                <h3 className="navbar-brand" >Dashboard</h3>
                <h5>Welcome {user} <button type="submit"className="btn_logout" onClick={remove}>Logout</button></h5>  
         
        </div>
        </nav>
        <Suspense fallback={<p className="Header_title">Still Loading…</p>}>
     <Newsapi />
     
    </Suspense>
   
        
        </div>
    );

};
const mapStateToProps = (state) => {
    return {
      user: state,
    };
  };
  
  const mapDispatchToProps = {
    UserLogout,
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
  ;
  