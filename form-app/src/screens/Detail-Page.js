import React, { Component } from 'react';
import Time from 'react-time-format';

 class DetailPage extends Component
{
    newsItem;
    constructor(props){
        super(props);
       this.newsItem = props.location.state.newsItem;
    }
    render(){
       
        return(
         <div>
               <nav className="navbar navbar-expand-lg navbar-light fixed-top">
    
                <h3 className="navbar-brand" >Dashboard</h3>
                </nav>
            <div className="detail_card">
            <div><h3 className="detail-title">{this.newsItem.title}</h3></div> 
            <div className="container">
                <div className="row">            
                <div className="card" >
                    <div className="card-body" >
                         <img src={this.newsItem.urlToImage} class="card-img" alt="..." width="20" height="300"/>
                         <h5 className="detail-card-author" ><strong>Author Name:</strong> {this.newsItem.author}</h5> 
                        <p className="card-text"><strong>Title: </strong>{this.newsItem.title}</p>
                        <p className="card-text"><strong>Description: </strong>{this.newsItem.description}</p>
                        <p className="card-text"><strong>Published At:</strong> <Time value={this.newsItem.publishedAt} format="YYYY-MM-DD hh:mm:ss" /></p>
                        
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
            )
    }
    }
 export default DetailPage;