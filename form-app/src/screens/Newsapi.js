import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Time from 'react-time-format'

import {fetchNewsArticles} from '../reducer/actions';


//const ApiUrl='https://newsapi.org/v2/top-headlines?q=trump&apiKey=c062f2ea288c44b4ad88f1439bf9aeed'
class Newsapi extends Component {

    constructor(props) {
        super(props);
        this.state = {
            articles: [],
            visible: 6,
            isLoading:true,
        }
        this.loadMore = this.loadMore.bind(this);
    }
    // Loader function
    loadMore() {
        this.setState((prev) => {
          return {visible: prev.visible + 6};
        });
      }

    //    Fetch Url data
    componentDidMount() {
       const { fetchNewsArticles } = this.props;
       fetchNewsArticles();
    }

    // handleOnNavigate () {
    //     articles
    //     history.push('/Detail-page', { state: { article: articles }})
    // }
  

    render() {
        console.log('this.props.articles', this.props.articles)
       
        return (
            <div>
                <p className="Header_title">NEWS API</p>
                {
              
               this.props.articles && this.props.articles.length > 0 && 
              this.props.articles.slice(0, this.state.visible).map((articles)=>
                <div class="card-deck">
                <div className="card">
                    <img className="card-img-top" src={articles.urlToImage} alt="News" />
                    <div className="card-body">
                        <h5 className="card-title" >{articles.title} </h5>
                         <Link to={{
                                        pathname: '/DetailPage',
                                        state: {
                                          newsItem : articles 
                                        }
                                    }}>
                                           
                        <button class="btn btn-primary" >Read More</button> </Link>
                        <p className="card-text"><small className="text-muted"><Time value={articles.publishedAt} format="YYYY-MM-DD hh:mm:ss"  />
                                    {/* {new Intl.DateTimeFormat('en-GB', {
                                    month: 'long', 
                                    day: '2-digit',
                                    year: 'numeric', 
                                    hour:'2-digit'
                                    
                                }).format(new Date(articles.publishedAt))} */}
                        </small></p>
                    </div>
                </div>
            </div> 
                    )}
                    <div>
                    {this.props.visible < this.props.articles.length &&
                        <button onClick={this.loadMore} type="button" className="load-more">Load more</button>
                     }
                    </div>
                
              
                </div>
            )
         }  
    }


const mapStateToProps = (state) => ({
    articles: state.articles
})      

const mapDispatchToProps = {
    fetchNewsArticles
}

export default connect(mapStateToProps, mapDispatchToProps)(Newsapi);