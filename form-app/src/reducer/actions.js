import { USER_REGISTER,SET_NEWS_ARTICLES,GET_NEWS_ARTICLES ,SET_NEWS_ARTICLES_ONERROR} from "./types";
import history from "./history";

export function setUserRegister(user) {
  console.log("setUserRegister");
  return {
    type: USER_REGISTER,
    user,
  };
}

export function CallUserRegister(data) {
  console.log("CallUserRegister outside", data);
  return (dispatch) => {
    console.log("CallUserRegister inside", data);
    dispatch(setUserRegister(data));
    localStorage.setItem("komal", JSON.stringify(data));
    alert("Registered successfully")
  };
}

export function CallUserLogin(data) {
  let details = JSON.parse(localStorage.getItem("komal"));
  if (
    data["email"] === details["email"] &&
    data["password"] === details["password"]
  ) {
    history.push("/Dashboard");
    console.log("home");
  } else {
    history.push("/sign-in");
    console.log("fail");
    alert("Invalid email or password");
  }
}

export function UserLogout() {
  console.log("bye");
  localStorage.clear();
  history.goBack("/");
}
export function getNewsArticles() {
  console.log("getNewsArticles");
  return {
    type: GET_NEWS_ARTICLES,
    
  };
}
export function setNewsArticles(articles) {
  console.log("setNewsArticles");
  return {
    type: SET_NEWS_ARTICLES,
    articlesList: articles
  };
}
export function setArticlesOnError(errors) {
  console.log("setArticlesOnError");
  return {
    type: SET_NEWS_ARTICLES_ONERROR,
    error: errors
  };
}

export function fetchNewsArticles() {
  return (dispatch) => {
    dispatch(getNewsArticles());
    fetch('https://newsapi.org/v2/top-headlines?q=trump&apiKey=c062f2ea288c44b4ad88f1439bf9aeed')
      .then((Response) => Response.json())
      .then((findresponse) => {
          console.log(findresponse.articles)
          dispatch(setNewsArticles(findresponse.articles))
      }).catch((err) => {
        dispatch(setArticlesOnError(err))
      })
  };
}