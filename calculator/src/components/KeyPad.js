import React , {Component } from  'react'

export default class KeyPad extends Component {

    onClick =(e)=>{
    this.props.onClick(e.target.name);
};
    state = {
        randNumber :[0,1,2,3,4,5,6,7,8,9],
    };
    
    uniqueNum;
    count;
    position = (e) =>{
        this.count=10;
        this.state.randNumber=[];
        this.props.onClick(e.target.name);

        while(this.count !==0){
            this.uniqueNum= Math.floor(Math.random() * 10) ;
            if(!this.state.randNumber.includes(this.uniqueNum)){
                console.log(this.uniqueNum);
                this.state.randNumber.push(this.uniqueNum);
                this.count--;
            } else{
                console.log(this.uniqueNum);
            }
        }
            this.copyArr=[...this.state.randNumber];
            this.setState({
                randNumber: this.copyArr,
            });
        };
    

   
    render() {
        return (
            <div className="buttons">
                <button name="("  onClick={this.onClick}>(</button>
                <button name="C"  onClick={this.onClick}>C</button>
                <button name="CE"  onClick={this.onClick}>CE</button>
                <button className="btn"  name=")"  onClick={this.onClick}>)</button>
               
                <button name={this.state.randNumber[1]} onClick={(e) => this.position(e)} >{this.state.randNumber[1]}</button>
                <button name={this.state.randNumber[2]} onClick={(e) => this.position(e)} >{this.state.randNumber[2]}</button>
                <button name={this.state.randNumber[3]} onClick={(e) => this.position(e)} >{this.state.randNumber[3]}</button>
                <button className="btn"  name="+" onClick={this.onClick}>+</button>

                <button name={this.state.randNumber[4]} onClick={(e) => this.position(e)} >{this.state.randNumber[4]}</button>
                <button name={this.state.randNumber[5]} onClick={(e) => this.position(e)} >{this.state.randNumber[5]}</button>
                <button name={this.state.randNumber[6]} onClick={(e) => this.position(e)} >{this.state.randNumber[6]}</button>
                <button  className="btn"  name="-" onClick={this.onClick}>-</button>

                <button name={this.state.randNumber[7]} onClick={(e) => this.position(e)} >{this.state.randNumber[7]}</button>
                <button name={this.state.randNumber[8]} onClick={(e) => this.position(e)} >{this.state.randNumber[8]}</button>
                <button name={this.state.randNumber[9]} onClick={(e) => this.position(e)} >{this.state.randNumber[9]}</button>
                <button  className="btn" name="*" onClick={this.onClick}>*</button>
               
                
                <button className="btn" name="."  onClick={this.onClick}>.</button>
                <button className="btn"  name={this.state.randNumber[0]} onClick={(e) => this.position(e)} >{this.state.randNumber[0]}</button>
                <button   className="btn" name="=" onClick={this.onClick}>=</button>
                <button  className="btn" name="/" onClick={this.onClick}>/</button>
            </div>
        );
    }
}
 