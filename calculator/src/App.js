
import './App.css';
import { Component } from 'react';
import KeyPad from './components/KeyPad.js';
import Output from './components/Output.js';

class App extends Component{
  
  state ={
    result :""
  };
   onClick=buttonName => {
     if(buttonName === '='){
       this.calculate()
     }else
     if(buttonName === 'C'){
       this.reset()
     }
     else if(buttonName === 'CE'){
       this.backspace();
     }
     else
     this.setState({
       result: this.state.result + buttonName
     });
   };
   backspace =() =>{
     this.setState({
       result: this.state.result.slice(0, -1)
     })
   }
   reset =() =>{
     this.setState({
       result :""
     })
   }
  
   calculate =() =>{
     try{
     this.setState({
       result : (eval (this.state.result) || "")+ ""
     });
    }catch(e){
      this.setState({
        result :"error"
      })
    }
   };
  render(){
    return(
      <div className="App">
        <div className="calc-body">
        <Output result={this.state.result}/>
        <KeyPad onClick={this.onClick} />
      </div>
      </div>
    );
  }
}

export default App;
