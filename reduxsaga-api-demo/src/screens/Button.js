import React from 'react';
import { connect } from 'react-redux';
import { getNews } from '../actions'

let styles = {
  backgroundColor: 'black',
  color:'white',
  width: '250px',
  height: '100px',
  display: 'block',
  margin: '50px auto',
  fontSize: '25px',
  border: '1px solid '
}

class Button extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hover: false };
  }
  render() {
    return (
      <button style={!this.state.hover ? styles : { ...styles, backgroundColor: 'cadetblue ' }}
        onMouseOut={() => { this.setState({ hover: false }) }}
        onMouseOver={() => { this.setState({ hover: true }) }}
        onClick={this.props.getNews}
      >Press to see News</button>
    );
  }

};

const mapDispatchToProps = {
  getNews: getNews,
};

Button = connect(null,mapDispatchToProps,)(Button);

export default Button;