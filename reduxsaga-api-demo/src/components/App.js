import React from 'react';
import Button from '../screens/Button';
import NewsItem from '../screens/NewsItem'
import Loading from '../screens/Loading'
let App = () => (
  <div>
  
     <Button />
     <Loading />
     <NewsItem />
  </div>
);
export default App;